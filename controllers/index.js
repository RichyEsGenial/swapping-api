const mysql = require('mysql');
const pool = require('../database');
const controller = {}

// GETS --------------------------------------------------------
controller.players = (req, res) => {

    pool.getConnection((err, conn) => {
        if(err) throw err;
        const sql = "SELECT * FROM Players";
        conn.query(sql, (err, result, fields) => {
            res.json({players: result})
        })        
        conn.release();
    })
};

controller.player = (req, res) => {

    pool.getConnection((err, conn) => {
        if(err) throw err;
        const sql = `SELECT * FROM Players WHERE UserName = ?`;
        const sql_formated = mysql.format(sql, [req.params.userName])
        conn.query(sql_formated, (err, result) => {
            if(err) throw err;
            res.json(result)
        })
        conn.release();
    })
};

controller.swapGroups = (req, res) => {

    pool.getConnection((err, conn) => {
        if(err) throw err;
        const sql = "SELECT * FROM SwapGroups";
        conn.query(sql, (err, result, fields) => {
            res.json(result);
        })
        conn.release();
    })
};

controller.swapping = (req, res) => {

    pool.getConnection((err, conn) => {
        if(err) throw err;
        const sql = "SELECT * FROM Swapping";
        conn.query(sql, (err, result, fields) => {
            res.json(result);
        })
        conn.release();
    })
}


// PUTS --------------------------------------------------------

controller.updateSwapping = (req, res) => {

    pool.getConnection((err, conn) => {
        if(err) throw err;
        const sql = "UPDATE Swapping SET FreePlayer = ?, SwapWith = ? WHERE IdSwapping = ?";
        const sql_formated = mysql.format(sql, [req.params.freePlayer, req.params.swapWith, req.params.idSwapping]); 
        conn.query(sql_formated, (err, result, fields) => {
            if(err) throw err;
            res.json(result);
        })
        conn.release();
    })
}

controller.updateSwapGroup = (req, res) => {

    pool.getConnection((err, conn) => {
        if(err) throw err;
        const sql = "UPDATE SwapGroups SET NameSwapGroup = ?, Private = ?, Password = ?, Status = ? WHERE IdSwapGroup = ?";
        const sql_formated = mysql.format(sql, [req.params.nameSwapGroup, req.params.private, req.params.password, req.params.status, req.params.idSwapGroup]); 
        conn.query(sql_formated, (err, result, fields) => {
            if(err) throw err;
            res.json(result);
        })
        conn.release();
    })
}
// POSTS -------------------------------------------------------

controller.addPlayer = (req, res) => {

    pool.getConnection((err, conn) => {
        if(err) throw err;
        const sql = `INSERT INTO Players (UserName, Password, Email) VALUES (?,?,?)`;
        const sql_formated = mysql.format(sql, [req.params.userName, req.params.password, req.params.email]);
        conn.query(sql_formated, (err, result, fields) => {
            if(err) throw err;
            res.json(result);
        })
        conn.release();
    })
}

controller.addSwapGroup = (req, res) => {

    pool.getConnection((err, conn) => {
        if(err) throw err;
        const sql = `INSERT INTO SwapGroups (NameSwapGroup, Private, Password, Owner) VALUES (?,?,?,?)`;
        const sql_formated = mysql.format(sql, [req.params.nameSwapGroup, req.params.private, req.params.password, req.params.owner]);
        conn.query(sql_formated, (err, result, fields) => {
            if(err) throw err;
            res.json(result);
        })
        conn.release();
    })
}

controller.addSwapping = (req, res) => {

    pool.getConnection((err, conn) => {
        if(err) throw err;
        const sql = `INSERT INTO Swapping (IdPlayer, IdSwap) VALUES (?,?)`;
        const sql_formated = mysql.format(sql, [req.params.idPlayer, req.params.idSwap]);
        conn.query(sql_formated, (err, result, fields) => {
            if(err) throw err;
            res.json(result);
        })
        conn.release();
    })
}

module.exports = controller;