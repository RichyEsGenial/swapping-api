const express = require('express');
const router = express.Router();
const controller = require('../controllers')

// GETS
router.get('/players', controller.players) //checked
router.get('/player/:userName', controller.player) //checked
router.get('/swapGroups', controller.swapGroups) //checked
router.get('/swapping', controller.swapping) //checked

// PUTS
router.put('/updateSwapping/:freePlayer/:swapWith/:idSwapping', controller.updateSwapping) //checked
router.put('/updateSwapGroup/:nameSwapGroup/:private/:password/:status/:idSwapGroup', controller.updateSwapGroup) //checked

// POSTS
router.post('/addPlayer/:userName/:password/:email?', controller.addPlayer) //checked
router.post('/addSwapGroup/:nameSwapGroup/:private/:password?/:owner', controller.addSwapGroup) //checked
router.post('/addSwapping/:idPlayer/:idSwap', controller.addSwapping) //checked

module.exports = router;