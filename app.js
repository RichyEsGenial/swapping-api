const express = require('express');
const port = process.env.PORT || 3001;
const app = express();

const router = require('./routes')

app.use(router)

app.listen(port)

